import RandomNumbers from "./Random-numbers/Random-numbers";
import {Component} from "react";

class App extends Component {
    state = {
        numbers: [0, 0, 0, 0, 0]
    };

    addNewNumber = () => {
        const numbersCopy = [];
        for (let i = 0; i < 5; i++) {
            let randomNumber = Math.floor(Math.random() * 36) + 5;
            while(numbersCopy.includes(randomNumber)) {
                randomNumber = Math.floor(Math.random() * 36) + 5;
            }
            numbersCopy.push(randomNumber);
            numbersCopy.sort((a, b) => a > b ? 1 : -1);
        }

        this.setState({
            numbers: numbersCopy
        })
    }
    render() {
        return (
            <div>
                <div className='btn'>
                    <button onClick={this.addNewNumber} className='button'>New number</button>
                </div>
                <div className='numbersBox'>
                    <RandomNumbers number={this.state.numbers[0]}/>
                    <RandomNumbers number={this.state.numbers[1]}/>
                    <RandomNumbers number={this.state.numbers[2]}/>
                    <RandomNumbers number={this.state.numbers[3]}/>
                    <RandomNumbers number={this.state.numbers[4]}/>
                </div>
            </div>
        );
    }
}

export default App;
