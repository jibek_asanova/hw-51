import React from 'react';
import './Random-numbers.css';

const RandomNumbers = props => {
    return (
        <div className='numbers'>
            <p>{props.number}</p>
        </div>
    );
};

export default RandomNumbers;